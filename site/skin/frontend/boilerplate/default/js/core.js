jQuery.noConflict();
(function( $ ) {

    var eventbinder = ( Modernizr.touch ) ? 'touchstart' : 'click';

    function sidebarInit(){

        /* create the sliding cart sidebar */

        $header_cart      = $('.header-container .cart');
        $layout_container = $('.layout');
        $layout_container.append('<aside class="cart" role="complementary">' + $header_cart.html() + '<aside>');

        function addIconFilter(){
            $('.search-button').after($('<div class="go-left fa fa-filter"></div>'));
        }

        /* create the sliding layered nav sidebar if found somewhere in the document */
        if (isLayered()) {
            $layout_container.prepend($layered = $('<div class="layered" role="complementary"></div>'));
            $layered.append($('.block-layered-nav'));

            /*
            Doing it the smart way, just add a class and everything adjusts :)
            Adjust the design if we have an extra column */

            $layout_3_cols = $('.layout-3-cols')
                /* fix layout the smart way */
                .addClass('layered')
                /* center design */
                .css('margin-left','-100%');

            addIconFilter(); /* add Filter icon to top bar*/

            $('.go-left').bind(eventbinder, function(e){
                e.preventDefault();
                $this = $('.layout');

                if ($this.hasClass('open-left')) {
                    $this.removeClass('open-left').addClass('closed');
                    $('.layout').css('margin-left','-100%');
                } else if ($this.hasClass('open-right')) {
                    $this.removeClass('open-right').addClass('open-left');
                    $('.layout').css('margin-left','0');
                } else {
                    $this.removeClass('closed').addClass('open-left');
                    $('.layout').css('margin-left','0');
                }
                if (!$layout_3_cols.hasClass('move-enabled')) $layout_3_cols.addClass('move-enabled');
            });

            $('.go-right').bind(eventbinder, function(e){
               e.preventDefault();
               $this = $('.layout');
               if ($this.hasClass('open-right')) {
                   $this.removeClass('open-right').addClass('closed');
                   $('.layout').css('margin-left','-100%');
                } else if ($this.hasClass('open-left')) {
                    $this.removeClass('open-left').addClass('open-right');
                    $('.layout').css('margin-left','-200%');
               } else {
                   $this.removeClass('closed').addClass('open-right');
                   $('.layout').css('margin-left','-200%');
               }

               /* enable transitions when swapping, do it now otherwise it will scroll on load */
               if (!$layout_3_cols.hasClass('move-enabled')) $layout_3_cols.addClass('move-enabled');
            });

        } else { // there is no left colum to scroll so bind normally, JUST the cart.

            $('.layout').addClass('move-enabled');

            $('.go-right').bind(eventbinder, function(e){
                e.preventDefault();
                $this = $(this);
                if ($this.hasClass('open-right')) {
                    $this.removeClass('open-right').addClass('closed');
                    $('.layout').css('margin-left','0');
                } else {
                    $this.removeClass('closed').addClass('open-right');
                    $('.layout').css('margin-left','-100%');
                }
            });

        }

    };

    function responsiveNavInit(){
        $('.nav-access, .close').bind(eventbinder, function(e){
            e.preventDefault();
            $this = $('.nav-access');
            if ($this.hasClass('open')) {
                $this.removeClass('open').addClass('closed');
                $('.responsive-site-container').removeClass('open');
            } else {
                $this.removeClass('closed').addClass('open');
                $('.responsive-site-container').addClass('open');
            }
        })
    }

    /* simple functions to determine current page */

    function isProductView(){
        return ($('.product-shop').length > 0);
    }

    function isLeftSidebar(){
        return ($('.col-left.sidebar').length > 0);
    }

    function isLayered(){
        return ($('.block-layered-nav').length > 0);
    }

    /* This moves some blocks in the DOM
     * Unfortunately this is necessary in a few cases.
     *
     *
     *
     *
     */
    function moveblocks(view){
       if (view == 'small') {

            if (isLeftSidebar()) {;
                $('.col-left.sidebar').appendTo('.col-main');
            }

            if (isLayered()) {
                $('.layered .layered').append($('.block-layered-nav'));
            }

        } else if (view == 'large') {

            if (isLeftSidebar()) {
                $('.col-main').before($('.col-left.sidebar'));
            }
            if (isLayered()) { 
                $('.col-left.sidebar').prepend($('.block-layered-nav'));
            }

        }
    }

    function bindResize(){
        /*
         * To do things when resizing:
         *
         * Change search button behaviour ... DONE
         * Close the slideout if open ... 
         * Pull the serchbar back in if it's out.
         * Needs much refining
         *
         *
         */
        $win        = $(window); /* somehow prototype does not like me using $w */
        $s          = $('.search-button');
        $header     = $('.responsive-site-container .header-container');
        $topsearch  = $('.quick-access').find('form');

        $topbar     = $('.quick-access');
        $popupbar   = $('.responsive-site-container .header-container .searchbar');

        var small  = false,
            medium = false,
            large  = false,
            xlarge = false;

            upperSearch = $('.header .quick-access').find('#search');
            lowerSearch = $('.searchbar .quick-access').find('#search');

        function wWidth(){
            return window.innerWidth;
        }

        $win.resize(function(){

            //document.title = wWidth();
            //console.log(large);

            if ((wWidth() < 450) && (!small)) {

                $s.unbind().bind(eventbinder, function(){

                    $popupbar.toggleClass('open').delay(250).find('input').focus();
                    $s.toggleClass('open');
                    if (!$popupbar.hasClass('open')) {
                        $input.blur();
                    }
                });

                $topbar.find('form').appendTo($popupbar);

                small = true;
                medium = false;
                large = false;

                moveblocks('small');

            }

            else if ((wWidth() >= 450) && (wWidth() < 960) && (!medium)) {
                $('.responsive-site-container').removeClass('open');
                $popupbar.find('form').appendTo($topbar);
                $s.unbind().bind(eventbinder, function(){
                    $topbar.find('form').submit();
                });

                /* close the bar if open */
                $s.removeClass('open');
                $popupbar.removeClass('open');

                small = false;
                medium = true;
                large = false;

                moveblocks('small');

            }

            else if ((wWidth() >= 960) && (!large)) {
                $('.responsive-site-container').removeClass('open');
                /* close the bar if open */
                $s.removeClass('open');
                $popupbar.removeClass('open');

                small = false;
                medium = false;
                large = true;

                moveblocks('large');
            }

        }).resize();

    } /* end bindresize */

    $(function() {

        $('.nav-container, .links-container').navgoco();
        responsiveNavInit();
        sidebarInit();
        bindResize();

        /* change next page specific js to broader js inits */
        if (isProductView()){
            $('#qty').spinner({
                'plusIcon'    : '/skin/frontend/boilerplate/default/images/spinner/qty_plus.png',
                'minusIcon'   : '/skin/frontend/boilerplate/default/images/spinner/qty_minus.png',
                'eventbind'   : 'click',
                'update'      : false
            });
        }
    });
})(jQuery);