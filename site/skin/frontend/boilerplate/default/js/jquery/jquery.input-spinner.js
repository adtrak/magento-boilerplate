jQuery.noConflict();

jQuery.fn.spinner = function( options ){

    var defaults = {

        plusIcon         : "plus.png",      // icons (plus)
        minusIcon        : "minus.png",     // icons (minus)
        update           : false,            // update the updatefield with the new price?
        updatefield      : "span.price",    // the field to update the price with
        priceAttr        : "price",         // the input attribute field to read for price
        maxLimit         : "limit-max",     // reads limit max attribute from input field
        maxValue         : 10,              // self explanatory
        minValue         : 1,               // self explanatory
        currency         : '£',             // self explanatory
        unitPrice        : null,            // if null will read priceAttr (redundant I know) otherwise will use this base price. TESTING
        updateOnChange   : true,            // update target field on change
        arrowControl     : true,            // use up down to increment, decrement
        eventbind        : 'click'
    };
    
    var settings = jQuery.extend( {}, defaults, options );

    /* Utility to format money 
    * Found this one!
    * 
    * -Andres
    */
    function formatMoney(number, places, symbol, thousand, decimal) {
        number = number || 0;
        places = !isNaN(places = Math.abs(places)) ? places : 2;
        symbol = symbol !== undefined ? symbol : "$";
        thousand = thousand || ",";
        decimal = decimal || ".";
        var negative = number < 0 ? "-" : "",
            i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
    }

    function checkField(val) {
        cond = true;
        cond = cond && !(val == null);
        cond = cond && !(isNaN(Number(val)));
        cond = cond && (val <= settings.maxValue);
        cond = cond && (val >= settings.minValue);
        return cond;
    }

    function placeSpinners(field){

        jPlus  = jQuery('<a href="#" class="qty-plus" title="Increase qty"><img src="' + settings.plusIcon + '" /></a>');
        jMinus = jQuery('<a href="#" class="qty-minus" title="Decrease qty"><img src="'+ settings.minusIcon +'" /></a>');
        jQuery(field).before( jMinus );
        jQuery(field).after( jPlus );

        //set maxValue according to the presence of the selector that detects per product max limit
        //settings.maxValue = ()
        settings.maxValue = (jQuery(field).attr(settings.maxLimit) == undefined) ? settings.maxValue : parseInt(jQuery(field).attr(settings.maxLimit));

        /* will also bind spinner to field */
        /* this can be condensed in only one binding, but for speed purposes I'm going to keep them separate */
        /* also binding can be done globally with a bit more effort */

        jPlus.on(settings.eventbind, function(e){
            e.preventDefault();
            unitPrice   = (settings.unitPrice == null) ? jQuery(field).attr(settings.priceAttr) : settings.unitPrice;

            current = (!checkField(jQuery(field).val())) ? 0 : jQuery(field).val();
            current = parseInt(current);

            if (current+1 <= settings.maxValue) current++;
            jQuery(field).val(current);
            targetField = (settings.updatefield == null) ? jQuery(field).parent().find('.price') : jQuery(settings.updatefield);
            if (settings.update) targetField.text(formatMoney(current * unitPrice,2, settings.currency));
        });

        jMinus.on(settings.eventbind, function(e){
            e.preventDefault();
            unitPrice   = (settings.unitPrice == null) ? jQuery(field).attr(settings.priceAttr) : settings.unitPrice;

            current = (!checkField(jQuery(field).val())) ? 1 : jQuery(field).val();
            current = parseInt(current);

            if (current-1 >= settings.minValue) current--;
            jQuery(field).val(current);
            targetField = (settings.updatefield == null) ? jQuery(field).parent().find('.price') : jQuery(settings.updatefield);
            if (settings.update) targetField.text(formatMoney(current * unitPrice,2, settings.currency));
        });

        if (settings.updateOnChange) {
            jQuery(field).bind('keyup', function(){

                unitPrice   = (settings.unitPrice == null) ? jQuery(field).attr(settings.priceAttr) : settings.unitPrice;
                unitPrice   = parseInt(unitPrice);

                if (!checkField(jQuery(field).val())) {

                    current = 1;
                    jQuery(field).css('border', '1px solid #cc0000');

                } else {

                    current = jQuery(field).val();
                    current = parseInt(current);
                    jQuery(field).css('border', '1px solid #e2e2e2e');

                } 

                jQuery(field).css('border')
                targetField = (settings.updatefield == null) ? jQuery(field).parent().find('.price') : jQuery(settings.updatefield);
                if (settings.update) targetField.text(formatMoney(current * unitPrice,2,'£'));
            })
        }

        if (settings.arrowControl) {

            jQuery(field).bind('keydown', function(e){

                unitPrice   = (settings.unitPrice == null) ? jQuery(field).attr(settings.priceAttr) : settings.unitPrice;
                unitPrice   = parseInt(unitPrice);

                if (e.keyCode == 38) { 
                    current = (!checkField(jQuery(field).val())) ? 0 : jQuery(field).val();
                    current = parseInt(current);

                    if (current+1 <= settings.maxValue) current++;
                    jQuery(field).val(current);
                    targetField = (settings.updatefield == null) ? jQuery(field).parent().find('.price') : jQuery(settings.updatefield);
                    if (settings.update) targetField.text(formatMoney(current * unitPrice,2, settings.currency));
                    return false;
                }

                if (e.keyCode == 40) { 
                    current = (!checkField(jQuery(field).val())) ? 1 : jQuery(field).val();
                    current = parseInt(current);

                    if (current-1 >= settings.minValue) current--;
                    jQuery(field).val(current);
                    targetField = (settings.updatefield == null) ? jQuery(field).parent().find('.price') : jQuery(settings.updatefield);
                    if (settings.update) targetField.text(formatMoney(current * unitPrice,2, settings.currency));
                }

            });
        }

    }

    return this.each(function() {
        sself = this;
        placeSpinners(sself);
    });

}